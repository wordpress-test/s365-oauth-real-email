<?php
/**
* Plugin Name: Simulate 365 OAuth Login - Get real email
* Plugin URI: https://www.cc-api.com
* Description: After user is logged in with Simulate 365 OAuth provider, this plubing will write his real email to user's profile (not @simulate365.com email).
* Version: 1.0.1
* Author: Mersad Katana
* Author URI: https://mersadk.dev/
**/